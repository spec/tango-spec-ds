.. TangoSpec documentation master file, created by
   sphinx-quickstart on Tue Mar 18 13:08:54 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to TangoSpec's documentation!
=====================================

TangoSpec is a TANGO_ device server which provides a TANGO_ interface to SPEC_.

.. toctree::
   :maxdepth: 2

   Getting started <getting_started.md>
   API <api>

.. _TANGO: http://www.tango-controls.org/
.. _PyTango: http://www.tinyurl.com/PyTango/
.. _SPEC: http://www.certif.com/
.. _SpecClientGevent: https://github.com/mxcube/specclient/
.. _Python: http://python.org/
.. _IPython: http://ipython.org/
.. _numpy: http://www.numpy.org/
.. _gevent: http://www.gevent.org/
.. _boost-python: http://www.boost.org/libs/python/
.. _PyPi: https://pypi.python.org/pypi/PyTango/
